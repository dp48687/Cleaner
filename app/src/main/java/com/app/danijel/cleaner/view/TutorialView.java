package com.app.danijel.cleaner.view;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.app.danijel.cleaner.shape.Rectangle;
import com.app.danijel.cleaner.util.SoundPlayer;

/**
 * Created by Danijel on 7/12/2017.
 */

public class TutorialView extends AbstractCustomView {

    public TutorialView(Context context, AppCompatActivity apa, int resource, int imageWidth, int imageHeight) {
        super(context, apa, resource, imageWidth, imageHeight);
    }

    @Override
    protected void makeAction(int actionIndex) {

    }

    @Override
    protected void makeRectangles(double widthScale, double heightScale) {
        Rectangle tutorial = new Rectangle(345 * widthScale, 741 * widthScale, 1357 * heightScale, 1465 * heightScale);
        rectangles.add(tutorial);
    }

}
