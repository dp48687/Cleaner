package com.app.danijel.cleaner.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Danijel on 7/12/2017.
 */

public class Database  extends SQLiteOpenHelper {
    public static String NAME = "puzz.db";
    private static String TABLE_NAME = "PUZZLE";
    private static String ID_ATTR = "ID";
    private static String SOLVED_ATTR = "SOLVED";
    private static String WIDTH_ATTR = "WIDTH";
    private static String HEIGHT_ATTR = "HEIGHT";
    private static String BODY_ATTR = "BODY";

    public Database(Context context) {
        super(context, NAME, null, 1);
    }

    /**
     * Initial creating database operation.
     * @param db
     * */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
                + ID_ATTR + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + SOLVED_ATTR + " SMALLINT NOT NULL, "
                + WIDTH_ATTR + " SMALLINT NOT NULL, "
                + HEIGHT_ATTR + " SMALLINT NOT NULL, "
                + BODY_ATTR + " TEXT NOT NULL"
                +");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * Inserts the puzzle into the existing database.
     * @param p an abstraction of a puzzle
     * */
    public void insert(Puzzle p) {
        short solved_ = p.isSolved() ? (short) 1 : (short) 0;
        getWritableDatabase().execSQL(
                "INSERT INTO " + TABLE_NAME + " ("
                        + SOLVED_ATTR + ", "
                        + WIDTH_ATTR + ", "
                        + HEIGHT_ATTR + ", "
                        + BODY_ATTR
                        + ") VALUES ("
                        + solved_ + ", "
                        + p.getWidth() + ", "
                        + p.getHeight() + ", "
                        + "'" + p.getBody() + "'"
                        + ")"
        );
    }

    /**
     * Used to update the existing puzzle in the databse.
     * @param newPuzzle puzzle which data is going to be used in the replacing
     * */
    public void modify(Puzzle newPuzzle) {
        int solved_ = newPuzzle.isSolved() ? 1 : 0;
        getWritableDatabase().execSQL(
                "UPDATE " + TABLE_NAME +
                        " SET " + BODY_ATTR + " = '" + newPuzzle.getBody() + "', " +
                                  SOLVED_ATTR + " = " + solved_ +
                        " WHERE " + ID_ATTR + " = " + newPuzzle.getId()
        );
    }

    /**
     * Retrieves the puzzles (all of them)
     * @return puzzles from database
     * */
    public List<Puzzle> getAllPuzzles() {
        List<Puzzle> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        if (!c.moveToFirst()) {
            c.close();
            db.close();
            return list;
        }
        while (c.moveToNext()) {
            String body = c.getString(c.getColumnIndex(BODY_ATTR));
            short id = (short) Integer.parseInt(c.getString(c.getColumnIndex(ID_ATTR)));
            short width = (short) Integer.parseInt(c.getString(c.getColumnIndex(WIDTH_ATTR)));
            short height = (short) Integer.parseInt(c.getString(c.getColumnIndex(HEIGHT_ATTR)));
            boolean solved = Integer.parseInt(c.getString(c.getColumnIndex(SOLVED_ATTR))) != 0;
            list.add(new Puzzle(id, width, height, solved, body));
        }
        c.close();
        db.close();
        return list;
    }

}
