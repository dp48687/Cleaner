package com.app.danijel.cleaner.observer;

/**
 * Created by Danijel on 8/2/2017.
 */

public interface OnFinishObserver {
    void onFinish();
}
