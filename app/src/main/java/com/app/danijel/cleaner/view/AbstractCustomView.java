package com.app.danijel.cleaner.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import com.app.danijel.cleaner.info.Info;
import com.app.danijel.cleaner.shape.Rectangle;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Danijel on 8/20/2016.
 */
public abstract class AbstractCustomView extends View implements View.OnTouchListener{
    private boolean check = true;
    private Bitmap bitmapToDraw;
    private Paint paint;
    public List<Rectangle> rectangles = new ArrayList<>();
    protected Context context;
    protected AppCompatActivity apa;
    private int resource, imageWidth, imageHeight;

    public AbstractCustomView(Context context, AppCompatActivity apa, int resource, int imageWidth, int imageHeight) {
        super(context);
        this.context = context;
        this.apa = apa;
        this.resource = resource;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        paint = new Paint();
        setOnTouchListener(this);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int viewWidth = getMeasuredWidth();
        int viewHeight = getMeasuredHeight();
        if (viewHeight > 0 && viewWidth > 0 && check) {
            bitmapToDraw = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), resource), viewWidth, viewHeight, true);
            Info.TRUE_HEIGHT = viewHeight;
            Info.TRUE_WIDTH = viewWidth;
            double widthScale = (viewWidth + 0.0) / imageWidth;
            double heightScale = (viewHeight + 0.0) / imageHeight;
            makeRectangles(widthScale, heightScale);
            check = false;
        }
        if (bitmapToDraw != null) {
            canvas.drawBitmap(bitmapToDraw, 0, 0, paint);
        }
        invalidate();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:{
                int index = isPointInShape(event.getX(),event.getY());
                makeAction(index);
                break;
            }
        }
        return false;
    }

    public int isPointInShape(double x, double y) {
        for (int i = 0; i<rectangles.size(); i++) {
            if (rectangles.get(i).containsPoint(x,y)) {
                return i;
            }
        }
        return -1;
    }

    public void setNewBackgroundPicture(int resourceID) {
        bitmapToDraw = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), resourceID), (int) Info.TRUE_WIDTH, (int) Info.TRUE_HEIGHT, true);
    }

    protected abstract void makeAction(int actionIndex);
    protected abstract void makeRectangles(double widthScale, double heightScale);

}
