package com.app.danijel.cleaner.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.app.danijel.cleaner.R;
import com.app.danijel.cleaner.util.SoundPlayer;
import com.app.danijel.cleaner.view.AbstractCustomView;
import com.app.danijel.cleaner.view.MainView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AbstractCustomView customView = new MainView(this, this, R.drawable.cleaner_front_off, 1550, 2270);
        setContentView(customView);
        SoundPlayer.startBackgroundMusic(this, R.raw.track1);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SoundPlayer.stopBackgroundMusic();
    }

}
