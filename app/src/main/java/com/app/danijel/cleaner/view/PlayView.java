package com.app.danijel.cleaner.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import com.app.danijel.cleaner.R;
import com.app.danijel.cleaner.database.Database;
import com.app.danijel.cleaner.info.Info;
import com.app.danijel.cleaner.logic.Board;
import com.app.danijel.cleaner.logic.DirectionConstants;
import com.app.danijel.cleaner.logic.FieldConstants;
import com.app.danijel.cleaner.logic.GameStateConstants;
import com.app.danijel.cleaner.logic.Player;
import com.app.danijel.cleaner.observer.OnFinishObserver;
import com.app.danijel.cleaner.observer.SoundObserver;
import com.app.danijel.cleaner.shape.Cell;
import com.app.danijel.cleaner.thread.AlgorithmThread;
import com.app.danijel.cleaner.thread.ScalingThread;
import com.app.danijel.cleaner.thread.SoundThread;
import com.app.danijel.cleaner.util.SoundPlayer;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Danijel on 7/29/2016.
 */
public class PlayView extends View implements View.OnTouchListener, OnFinishObserver, SoundObserver {
    public static final int PLAYER_COLOR = Color.argb(255, 255, 255, 255);
    public static final int STONE_COLOR = Color.argb(255, 0, 0, 0);
    public static final int INNER_FIELD_COLOR = Color.argb(255, 0, (int) (0.3 * 255), 255);
    public static final int OUTER_FIELD_COLOR = Color.argb(255, 0, 0, (int) (0.7 * 255));
    public static final int CLEANED_FIELD_COLOR = Color.argb(255, (int) (0.475 * 255), (int) (0.718 * 255), 255);
    public static final int GAME_LOST_FIELD_COLOR = Color.rgb(200, 0, 0);
    public static final int GAME_WON_FIELD_COLOR = Color.rgb(0, 200, 0);
    public static final int GAME_RUNNING_FIELD_COLOR = Color.rgb(0, 0, 200);
    public static final int MAX_ANIMATION_DURATION = 30;
    private static final float MIN_DISTANCE = 20f;
    private static final int FINISH_COUNTDOWN_TIME_MILLIS = 2500;
    private float x1, x2, y1, y2;
    private int screenX, screenY;
    private int cellWidth, cellHeight;
    public Cell cellField[][];
    private Paint paint = new Paint();
    public Board b;
    private boolean initialized;
    private Database db;
    public Player p;
    public GameStateConstants state = GameStateConstants.GAME_STILL_RUNNING;
    private AppCompatActivity apa;
    private Context context;
    public DirectionConstants directionSelected = DirectionConstants.UNDEFINED;
    private SoundThread soundThread;
    public ConcurrentLinkedQueue<Cell> scaledCells = new ConcurrentLinkedQueue<>();
    private ScalingThread scalingThread;
    private AlgorithmThread algorithmThread;

    public PlayView(Context context, AppCompatActivity apa) {
        super(context);
        this.context = context;
        this.apa = apa;
        this.p = new Player();
        setOnTouchListener(this);
        algorithmThread = new AlgorithmThread(this);
        soundThread = new SoundThread(context, R.raw.barrier, algorithmThread);
        scalingThread = new ScalingThread(MAX_ANIMATION_DURATION, scaledCells);
        db = new Database(context);
        b = Info.currentPuzzle.getBoard();
        checkPlayer();
        Cell.setFrameColor(GAME_RUNNING_FIELD_COLOR);
    }

    private void init() {
        Info.HORIZONTAL_OFFSET = (int) (0.05 * screenX);
        Info.VERTICAL_OFFSET = (int) (0.05 * screenY);
        cellWidth = (int) ((screenX - 2 * Info.HORIZONTAL_OFFSET) / Info.WIDTH);
        cellHeight = (int) ((screenY - 2 * Info.VERTICAL_OFFSET) / Info.HEIGHT);
        cellField = new Cell[b.getField().length][b.getField()[0].length];
        for (short i = 0; i < Info.HEIGHT; i++) {
            for (short j = 0; j < Info.WIDTH; j++) {
                int color = PLAYER_COLOR;
                switch (b.getIdentifier(j, i)) {
                    case OUTER: color = OUTER_FIELD_COLOR; break;
                    case CLEANED: color = CLEANED_FIELD_COLOR; break;
                    case STONE: color = STONE_COLOR; break;
                    case INNER: color = INNER_FIELD_COLOR; break;
                }
                cellField[i][j] = new Cell((int) (j * cellWidth + Info.HORIZONTAL_OFFSET), (int) (i * cellHeight + Info.VERTICAL_OFFSET), cellWidth, cellHeight, color);
            }
        }
    }

    private void checkPlayer() {
        b.getField()[0][0] = FieldConstants.PLAYER;
    }

    private void drawCells(Canvas c, Paint p) {
        for (int i = 0; i < b.getField().length; i++) {
            for (int j = 0; j < b.getField()[0].length; j++) {
                cellField[i][j].draw(c, p);
            }
        }
        for (Cell cx : scaledCells) {
            cx.draw(c, p);
        }
    }

    private void drawBackground(Canvas c, Paint p) {
        p.setColor(CLEANED_FIELD_COLOR);
        c.drawRect(0, 0, screenX, screenY, p);
    }

    private void processMove() {
        algorithmThread = new AlgorithmThread(this);
        algorithmThread.start();
    }

    @Override
    public void onFinish() {
        SoundPlayer.continueBackgroundMusic(getContext(), R.raw.track1);
        algorithmThread.interrupt();
        soundThread.interrupt();
        scalingThread.interrupt();
        apa.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String toDisplay = state.equals(GameStateConstants.GAME_LOST) ? "YOU LOST!" : "YOU WON!";
                Toast.makeText(getContext(), toDisplay, Toast.LENGTH_LONG).show();
                if (toDisplay.equals("YOU WON!") && !Info.currentPuzzle.isSolved()) {
                    Info.currentPuzzle.setSolved(true);
                    db.modify(Info.currentPuzzle);
                    db.close();
                }
                Cell.setFrameColor(Color.rgb(200, 0, 0));
                if (state.equals(GameStateConstants.GAME_STILL_RUNNING)) {
                    apa.finish();
                } else {
                    new Thread(
                            new Runnable() {
                                @Override
                                public void run() {
                                    int color = state.equals(GameStateConstants.GAME_LOST) ? GAME_LOST_FIELD_COLOR : GAME_WON_FIELD_COLOR;
                                    Cell.setFrameColor(color);
                                    try {
                                        Thread.sleep(FINISH_COUNTDOWN_TIME_MILLIS);
                                    } catch (InterruptedException e) {

                                    }
                                    apa.finish();
                                }
                            }
                    ).start();
                }
            }
        });
    }

    @Override
    public void playSound() {
        try {
            soundThread.interrupt();
            soundThread = new SoundThread(context, R.raw.barrier, algorithmThread);
            soundThread.start();
        } catch (Exception e) {

        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (screenX == 0 || screenY == 0) {
            screenX = getMeasuredWidth();
            screenY = getMeasuredHeight();
        } else {
            if (!initialized) {
                init();
                algorithmThread.setScreenDimensions(screenX, screenY);
                scalingThread.start();
                initialized = true;
            }
            drawBackground(canvas, paint);
            drawCells(canvas, paint);
        }
        invalidate();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (!state.equals(GameStateConstants.GAME_STILL_RUNNING) || algorithmThread.isRunning()) {
            return false;
        }
        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                x1 = event.getX();
                y1 = event.getY();
                return true;
            }
            case MotionEvent.ACTION_UP: {
                x2 = event.getX();
                y2 = event.getY();
                float deltaX = x2 - x1;
                float deltaY = y2 - y1;
                if (Math.abs(deltaX) < MIN_DISTANCE && Math.abs(deltaY) < MIN_DISTANCE) {
                    return false;
                } else {
                    if (Math.abs(deltaX) > Math.abs(deltaY)) {
                        directionSelected = (deltaX > 0) ? DirectionConstants.EAST : DirectionConstants.WEST;
                    } else {
                        directionSelected = (deltaY > 0) ? DirectionConstants.SOUTH : DirectionConstants.NORTH;
                    }
                    if (!directionSelected.equals(DirectionConstants.UNDEFINED)) {
                        processMove();
                    }
                    if (!state.equals(GameStateConstants.GAME_STILL_RUNNING)) {
                        onFinish();
                    }
                }
            }
            break;
        }
        return false;
    }

}
