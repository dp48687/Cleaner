package com.app.danijel.cleaner.shape;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Danijel on 7/30/2017.
 */

public interface Drawable {
    void draw(Canvas c, Paint p);
}
