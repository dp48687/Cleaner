package com.app.danijel.cleaner.shape;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by Danijel on 7/30/2017.
 */

public class Cell implements Drawable {
    private int x0, y0, width, height;
    private char offset = 3;
    private int internalColor;
    private static int frameColor = Color.rgb(0, 0, 200);
    private float dW, dH;

    public Cell(int x0, int y0, int width, int height, int internalColor) {
        this.x0 = x0;
        this.y0 = y0;
        this.width = width;
        this.height = height;
        this.internalColor = internalColor;
    }

    @Override
    public void draw(Canvas c, Paint p) {
        p.setStyle(Paint.Style.FILL);
        p.setColor(frameColor);
        c.drawRect(x0 - (int) (dW / 2.0), y0 - (int) (dH / 2.0), x0 + width + (int) (dW / 2.0), y0 + height + (int) (dH / 2.0), p);
        p.setColor(internalColor);
        c.drawRect(x0 + offset - (int) (dW / 2.0), y0 + offset - (int) (dH / 2.0), x0 + width - offset + (int) (dW / 2.0), y0 + height - offset + (int) (dH / 2.0), p);
    }

    public void setInternalColor(int internalColor) {
        this.internalColor = internalColor;
    }

    public static void setFrameColor(int frameColor) {
        Cell.frameColor = frameColor;
    }

    public float getDW() {
        return dW;
    }

    public float getDH() {
        return dH;
    }

    public void setScaleSizes(float dW, float dH) {
        this.dW = dW;
        this.dH = dH;
    }

}
