package com.app.danijel.cleaner.shape;

/**
 * Created by Danijel on 8/21/2016.
 */
public class Rectangle {
    public double x1, x2, y1, y2;

    public Rectangle(double x1, double x2, double y1, double y2) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
    }

    public boolean containsPoint(double x, double y) {
        if (x < x2 && x > x1 && y < y2 && y > y1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "(x1,y1) = " + "(" + x1 + ", " + y1 + ")" + "\n" + "(x2,y2) = " + "(" + x2 + ", " + y2 + ")";
    }

}
