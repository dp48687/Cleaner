package com.app.danijel.cleaner.thread;

import com.app.danijel.cleaner.shape.Cell;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Danijel on 8/2/2017.
 */

public class ScalingThread extends Thread {
    private ConcurrentLinkedQueue<Cell> scaledCells;
    private boolean isPaused;
    private final Object lockObject = new Object();
    private int scalingAnimationDuration;

    public ScalingThread(int scalingAnimationDuration, ConcurrentLinkedQueue<Cell> scaledCells) {
        this.scalingAnimationDuration = scalingAnimationDuration;
        this.scaledCells = scaledCells;
    }

    @Override
    public void run() {
        float decrementConstant = 0.4f;
        while (true) {
            synchronized (lockObject) {
                while (isPaused) {
                    try {
                        lockObject.wait();
                    } catch (InterruptedException e) {

                    }
                }
            }
            try {
                Thread.sleep(scalingAnimationDuration);
                for (Cell c : scaledCells) {
                    if (c.getDW() - decrementConstant <= 0.0) {
                        c.setScaleSizes(0.0f, 0.0f);
                        scaledCells.remove(c);
                    } else {
                        c.setScaleSizes(c.getDW() - decrementConstant, c.getDH() - decrementConstant);
                    }
                }
            } catch (InterruptedException e) {

            }
        }
    }

    public void onPause() {
        synchronized (lockObject) {
            isPaused = true;
        }
    }

    public void onResume() {
        synchronized (lockObject) {
            isPaused = false;
            lockObject.notifyAll();
        }
    }

}
