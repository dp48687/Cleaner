package com.app.danijel.cleaner.database;

import com.app.danijel.cleaner.logic.Board;

/**
 * Created by Danijel on 7/13/2017.
 */

/**
 * Represents the abstraction of the puzzle.
 * */
public class Puzzle {
    /**
     * Unique number which identifies puzzle.
     * */
    private int id;

    /**
     * The number of columns and rows of board matrix, respectively.
     * */
    private int width, height;

    /**
     * Is puzzle already solved by the player?
     * */
    private boolean solved;

    /**
     * A primitive, string representation of this puzzle.
     * */
    private String body;

    /**
     * Initializes all of the parameters.
     * */
    public Puzzle(int id, int width, int height, boolean solved, String body) {
        this.id = id;
        this.width = width;
        this.height = height;
        this.body = body;
        this.solved = solved;
    }

    /**
     * A bunch of getters and setters to finish puzzle functionality.
     * */

    public int getId() {
        return id;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isSolved() {
        return solved;
    }

    public String getBody() {
        return body;
    }

    public Board getBoard() {
        return new Board(body);
    }

    public void setSolved(boolean solved) {
        this.solved = solved;
    }

}
