package com.app.danijel.cleaner.logic;

/**
 * Created by Danijel on 7/12/2017.
 */

/**
 * Describes the current game state.
 * The names are pretty self-explainable
 * */
public enum GameStateConstants {
    GAME_WON,
    GAME_LOST,
    GAME_STILL_RUNNING
}
