package com.app.danijel.cleaner.thread;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Created by Danijel on 8/2/2017.
 */

public class SoundThread extends Thread {
    private MediaPlayer mp;
    private int resource;
    private Context c;
    private AlgorithmThread at;

    public SoundThread(Context c, int resource, final AlgorithmThread at) {
        this.c = c;
        this.resource = resource;
        this.at = at;
        mp = MediaPlayer.create(c, resource);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
            public void onCompletion(MediaPlayer player) {
                mp.release();
                mp = null;
            }
        });
    }

    @Override
    public void run() {
        playSound();
        while (mp.isPlaying()) {
            try {
                Thread.sleep(25);
            } catch (InterruptedException e) {

            }
        }
        mp.release();
        at.onResume();
    }

    private void playSound() {
        try {
            if (mp != null && mp.isPlaying()) {
                mp.stop();
                mp.reset();
            } else {
                mp = MediaPlayer.create(c, resource);
            }
            mp.start();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}
