package com.app.danijel.cleaner.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.app.danijel.cleaner.R;
import com.app.danijel.cleaner.database.Puzzle;
import com.app.danijel.cleaner.info.Info;
import com.app.danijel.cleaner.util.SoundPlayer;
import com.app.danijel.cleaner.view.CustomListAdapter;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        final ListView v = (ListView) findViewById(R.id.lv);
        v.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        final CustomListAdapter cla = new CustomListAdapter(this);
        v.setAdapter(cla);
        v.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Info.currentPuzzle = (Puzzle) v.getAdapter().getItem(position);
                Intent i = new Intent(cla.getContext(), PlayActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                cla.getContext().startActivity(i);
                SoundPlayer.pauseBackgroundMusic(cla.getContext(), R.raw.track1);
            }
        });
    }

}
