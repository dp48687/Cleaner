package com.app.danijel.cleaner.view;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.app.danijel.cleaner.R;
import com.app.danijel.cleaner.activity.LikeActivity;
import com.app.danijel.cleaner.activity.ListActivity;
import com.app.danijel.cleaner.activity.TutorialActivity;
import com.app.danijel.cleaner.database.Database;
import com.app.danijel.cleaner.database.Puzzle;
import com.app.danijel.cleaner.info.Info;
import com.app.danijel.cleaner.shape.Rectangle;
import com.app.danijel.cleaner.util.SoundPlayer;

/**
 * Created by Danijel on 8/21/2016.
 */
public class MainView extends AbstractCustomView {
    private Database db;

    public MainView(Context context, AppCompatActivity apa, int resource, int imageWidth, int imageHeight) {
        super(context, apa, resource, imageWidth, imageHeight);
        apa.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        try {
            db = new Database(context);
            if (db.getAllPuzzles().size() == 0) {
                db.insert(new Puzzle(1, 10, 6, false, "10x6*3,4,3,5"));
                db.insert(new Puzzle(1, 10, 8, false, "10x8*5,5,4,7"));
                db.insert(new Puzzle(1, 10, 8, false, "10x8*4,3,4,4,5,8"));
                db.insert(new Puzzle(1, 10, 10, false, "10x10*7,4,5,7,3,8"));
                db.insert(new Puzzle(1, 12, 10, false, "12x10*3,2,5,3,4,5,2,6,7,10,7,9"));
                db.insert(new Puzzle(1, 10, 8, false, "10x8*6,3,5,5,4,7,3,9"));
                db.insert(new Puzzle(1, 9, 6, false, "9x6*5,2,3,6,4,8"));
                db.insert(new Puzzle(1, 15, 10, false, "15x10*9,4,9,5"));
                db.insert(new Puzzle(1, 10, 7, false, "10x7*2,6,8,1,8,3,8,5,8,7"));
                db.close();
            }
        } catch (Exception e) {
            Toast.makeText(context, "Failed to create database. Go to SETTINGS -> STORAGE and make sure mass storage only IS NOT selected.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void makeRectangles(double widthScale, double heightScale) {
        Rectangle play = new Rectangle(537 * widthScale, 985 * widthScale, 601 * heightScale, 749 * heightScale);
        Rectangle options = new Rectangle(397 * widthScale, 1133 * widthScale, 865 * heightScale, 1061 * heightScale);
        Rectangle about = new Rectangle(485 * widthScale, 1073 * widthScale, 1125 * heightScale, 1281 * heightScale);
        Rectangle like = new Rectangle(593 * widthScale, 925 * widthScale, 1417 * heightScale, 1577 * heightScale);
        Rectangle exit = new Rectangle(585 * widthScale, 953 * widthScale, 1701 * heightScale, 1893 * heightScale);
        rectangles.add(play);
        rectangles.add(options);
        rectangles.add(about);
        rectangles.add(like);
        rectangles.add(exit);
    }

    @Override
    protected void makeAction(int action) {
        switch (action) {
            case 0:{
                context.startActivity(new Intent(context, ListActivity.class));
                break;
            }
            case 1:{
                context.startActivity(new Intent(context, TutorialActivity.class));
                break;
            }
            case 2:{
                Info.BACKGROUND_MUSIC_ENABLED = !Info.BACKGROUND_MUSIC_ENABLED;
                if (Info.BACKGROUND_MUSIC_ENABLED) {
                    SoundPlayer.continueBackgroundMusic(context, R.raw.track1);
                    setNewBackgroundPicture(R.drawable.cleaner_front_on);
                } else {
                    SoundPlayer.pauseBackgroundMusic(context, R.raw.track1);
                    setNewBackgroundPicture(R.drawable.cleaner_front_off);
                }
                break;
            }
            case 3:{
                context.startActivity(new Intent(context, LikeActivity.class));
                break;
            }
            case 4:{
                SoundPlayer.stopBackgroundMusic();
                apa.finish();
                break;
            }
        }
    }

}
