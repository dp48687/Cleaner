package com.app.danijel.cleaner.view;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.danijel.cleaner.R;
import com.app.danijel.cleaner.database.Database;
import com.app.danijel.cleaner.database.Puzzle;
import com.app.danijel.cleaner.info.Info;
import java.util.List;

/**
 * Created by Danijel on 7/13/2017.
 */

public class CustomListAdapter extends BaseAdapter {
    private Puzzle[] puzzles;
    private Puzzle current;
    private Context context;
    private ViewHolder vh;
    private Database db;
    private LayoutInflater layoutInflater;

    public CustomListAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        init();
    }

    private void init() {
        db = new Database(context);
        List<Puzzle> puzzles = db.getAllPuzzles();
        this.puzzles = new Puzzle[puzzles.size()];
        for (short i = 0; i < puzzles.size(); i++) {
            this.puzzles[i] = puzzles.get(i);
        }
    }

    @Override
    public int getCount() {
        return puzzles.length;
    }

    @Override
    public Object getItem(int position) {
        return puzzles[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View dummyView = layoutInflater.inflate(R.layout.puzzle_list_view, parent, false);
        if (vh == null) {
            ImageView img = (ImageView) dummyView.findViewById(R.id.imageView2);
            TextView tv = (TextView) dummyView.findViewById(R.id.description);
            vh = new ViewHolder(tv, img);
        }
        vh.description = (TextView) dummyView.findViewById(R.id.description);       // this view holder pattern
        vh.image = (ImageView) dummyView.findViewById(R.id.imageView2);             // does not work
        current = puzzles[position];
        if (!current.isSolved()) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                vh.image.setImageDrawable(context.getResources().getDrawable(android.R.drawable.btn_star_big_off, context.getApplicationContext().getTheme()));
            } else {
                vh.image.setImageDrawable(context.getResources().getDrawable(android.R.drawable.btn_star_big_off));
            }
        }
        Info.currentPuzzle = current;
        SpannableString boldText = new SpannableString(Integer.toString(position + 1));
        boldText.setSpan(new StyleSpan(Typeface.BOLD), 0, boldText.length(), 0);
        vh.description.setText("");
        vh.description.append("\n     ");
        vh.description.append(boldText);
        vh.description.append("   " + current.getWidth() + "x" + current.getHeight());
        Info.HEIGHT = (short) current.getHeight();
        Info.WIDTH = (short) current.getWidth();
        return dummyView;
    }

    public Context getContext() {
        return context;
    }

    private static class ViewHolder {
        private TextView description;
        private ImageView image;

        public ViewHolder(TextView description, ImageView image) {
            this.description = description;
            this.image = image;
        }

    }

}
