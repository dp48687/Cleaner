package com.app.danijel.cleaner.logic;

/**
 * Created by Danijel on 7/11/2017.
 */

/**
 * A simple constants which describe the specific element in the board.
 * REDIRECTORS ARE NOT USED IN THE LOGIC YET.
 * */
public enum FieldConstants {
    INNER,
    OUTER,
    CLEANED,
    PLAYER,
    STONE,
    REDIRECTOR_NORTH,
    REDIRECTOR_SOUTH,
    REDIRECTOR_EAST,
    REDIRECTOR_WEST
}
