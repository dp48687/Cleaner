package com.app.danijel.cleaner.thread;

import com.app.danijel.cleaner.info.Info;
import com.app.danijel.cleaner.logic.Board;
import com.app.danijel.cleaner.logic.DirectionConstants;
import com.app.danijel.cleaner.logic.FieldConstants;
import com.app.danijel.cleaner.logic.GameStateConstants;
import com.app.danijel.cleaner.logic.Player;
import com.app.danijel.cleaner.view.PlayView;

/**
 * Created by Danijel on 8/2/2017.
 */

public class AlgorithmThread extends Thread {
    private boolean isPaused;
    private final Object lockObject = new Object();
    private int screenX, screenY;
    private DirectionConstants directionSelected = DirectionConstants.UNDEFINED;
    private PlayView pw;
    private boolean running;

    public AlgorithmThread(PlayView pw) {
        this.pw = pw;
        this.directionSelected = pw.directionSelected;
    }

    @Override
    public void run() {
        running = true;
        do {
            synchronized (lockObject) {
                while (isPaused) {
                    try {
                        lockObject.wait();
                    } catch (InterruptedException e) {

                    }
                }
            }
            pw.state = processMove(pw.b, pw.p, directionSelected);
            for (int i = pw.p.y - 1; i <= pw.p.y + 1; i++) {
                if (i < 0 || i >= Info.HEIGHT) {
                    continue;
                }
                for (int j = pw.p.x - 1; j <= pw.p.x + 1; j++) {
                    if (j < 0 || j >= Info.WIDTH) {
                        continue;
                    }
                    int color = Info.PLAYER_COLOR;
                    switch (pw.b.getIdentifier((short)j, (short)i)) {
                        case OUTER:
                            color = Info.OUTER_FIELD_COLOR;
                            break;
                        case CLEANED:
                            color = Info.CLEANED_FIELD_COLOR;
                            break;
                        case STONE:
                            color = Info.STONE_COLOR;
                            break;
                        case INNER:
                            color = Info.INNER_FIELD_COLOR;
                            break;
                    }
                    pw.cellField[i][j].setInternalColor(color);
                }
            }
            if (pw.state.equals(GameStateConstants.GAME_LOST) || pw.state.equals(GameStateConstants.GAME_WON)) {
                pw.onFinish();
            }
            if (pw.p.isConstraintAhead()) {
                break;
            }
            pw.scaledCells.add(pw.cellField[pw.p.y][pw.p.x]);
            pw.cellField[pw.p.y][pw.p.x].setScaleSizes(Info.MAX_CELL_INCREMENT, Info.MAX_CELL_INCREMENT);
            pw.playSound();
            onPause();
        } while (pw.p.getCurrentState().equals(FieldConstants.INNER) && pw.state.equals(GameStateConstants.GAME_STILL_RUNNING));
        running = false;
        interrupt();
    }

    public void setScreenDimensions(int width, int height) {
        screenX = width;
        screenY = height;
    }

    /**
     * Processes the moving of a player across the board.
     * @param b board
     * @param p player
     * @param direction selected direction
     * @return GAME_LOST if player is stuck in board,
     *         GAME_WON if player has cleaned up whole board,
     *         GAME_STILL_RUNNING if player hasn't cleaned up the board but not stuck in it
     * */
    private GameStateConstants processMove(Board b, Player p, DirectionConstants direction) {
        p.move(direction, b);
        if (!p.hasLegalMoves(b)) {
            return GameStateConstants.GAME_LOST;
        }
        if (b.isCleanedUp()) {
            return GameStateConstants.GAME_WON;
        }
        return GameStateConstants.GAME_STILL_RUNNING;
    }

    public boolean isRunning() {
        return running;
    }

    private void onPause() {
        synchronized (lockObject) {
            isPaused = true;
        }
    }

    void onResume() {
        synchronized (lockObject) {
            isPaused = false;
            lockObject.notifyAll();
        }
    }

}
