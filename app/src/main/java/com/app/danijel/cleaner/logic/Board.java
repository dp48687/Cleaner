package com.app.danijel.cleaner.logic;

import com.app.danijel.cleaner.info.Info;

/**
 * Created by Danijel on 7/11/2017.
 */

/**
 * An abstraction of the board.
 * */
public class Board {

    /**
     * A field of identifiers which together define board.
     * */
    private FieldConstants[][] field;

    /**
     * Initializes the string body of the board.
     * @param info primitive representation of the board.
     * */
    public Board(String info) {
        parseBoard(info);
        Info.HEIGHT = (short) field.length;
        Info.WIDTH = (short) field[0].length;
    }

    /**
     * Inspects if this board is finished. WARNING: O(n^2) complexity.
     * @return is this board cleaned up?
     * */
    public boolean isCleanedUp() {
        for (short i = 0; i < Info.HEIGHT; i++) {
            for (short j = 0; j < Info.WIDTH; j++) {
                if (field[i][j].equals(FieldConstants.INNER)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Gets the board value at (y, x)
     * @param x a horizontal index
     * @param y a vertical index
     * @return value in form of a <code>FieldConstants</code> constant.
     * */
    public FieldConstants getIdentifier(short x, short y) {
        if (x < 0 || x >= Info.WIDTH || y < 0 || y >= Info.HEIGHT) {
            return null;
        } else {
            return field[y][x];
        }
    }

    /**
     * Sets the board value at (y, x)
     * @param x a horizontal index
     * @param y a vertical index
     * @param constant a new value which will overwrite the existing one at position specified in parameters.
     * */
    public void setIdentifier(short x, short y, FieldConstants constant) {
        if (x >= 0 && x < Info.WIDTH && y >= 0 && y < Info.HEIGHT) {
            field[y][x] = constant;
        }
    }

    /**
     * Retrieves the information at each position to build the field.
     * Format(for example) - "wxh*stoneCoordinates"
     * w - width
     * h - height
     * o - outer
     * i - inner
     * p - player
     * c - cleaned
     * t - stone
     * @param boardInfo info
     * */
    private void parseBoard(String boardInfo) {
        String[] parsed = boardInfo.replace("\n", "").replace(" ", "").split("\\*");
        java.util.Scanner jsc = new java.util.Scanner(parsed[0]);
        jsc.useDelimiter("x");
        int width = jsc.nextInt();
        int height = jsc.nextInt();
        field = new FieldConstants[height + 2][width + 2];
        for (short i = 0; i < height + 2; i++) {
            for (short j = 0; j < width + 2; j++) {
                if (i == 0 || j == 0 || i == height + 1 || j == width + 1) {
                    field[i][j] = FieldConstants.OUTER;
                } else {
                    field[i][j] = FieldConstants.INNER;
                }
            }
        }
        field[0][0] = FieldConstants.PLAYER;
        jsc = new java.util.Scanner(parsed[1]);
        jsc.useDelimiter(",");
        while (jsc.hasNext()) {
            field[Integer.parseInt(jsc.next())][Integer.parseInt(jsc.next())] = FieldConstants.STONE;
        }
    }

    /**
     * A simple getter whic retrieves the board.
     * @return field
     * */
    public FieldConstants[][] getField() {
        return field;
    }

}
