package com.app.danijel.cleaner.info;

import android.graphics.Color;
import com.app.danijel.cleaner.database.Puzzle;

/**
 * Created by Danijel on 7/10/2017.
 */

public class Info {
    /**
     * Width and height of the matrix board
     * */
    public static int WIDTH, HEIGHT;

    /**
     * An offset related to rendering.
     * */
    public static float VERTICAL_OFFSET, HORIZONTAL_OFFSET;

    /**
     * Width of a screen.
     * */
    public static float TRUE_WIDTH;

    /**
     * Height of a screen.
     * */
    public static float TRUE_HEIGHT;

    /**
     * The list adapter sets the current puzzle which is going to be played.
     * */
    public static Puzzle currentPuzzle;

    /**
     * A players segment on a board.
     * */
    public static final int PLAYER_COLOR = Color.argb(255, 255, 255, 255);

    /**
     * Determines the color of the stones.
     * */
    public static final int STONE_COLOR = Color.argb(255, 0, 0, 0);

    /**
     * Determines the color of inner fields which aren't yet visited.
     * */
    public static final int INNER_FIELD_COLOR = Color.argb(255, 0, (int) (0.3 * 255), 255);

    /**
     * Determines the color of inner fields which are free to walk on.
     * */
    public static final int OUTER_FIELD_COLOR = Color.argb(255, 0, 0, (int) (0.7 * 255));

    /**
     * Determines the color of inner fields which are needed to be stepped over.
     * */
    public static final int CLEANED_FIELD_COLOR = Color.argb(255, (int) (0.475 * 255), (int) (0.718 * 255), 255);

    /**
     * Another thing related to rendering the scene.
     * */
    public static final float MAX_CELL_INCREMENT = 25.0f;

    /**
     * Determines if the music is supposed to be played.
     * */
    public static boolean BACKGROUND_MUSIC_ENABLED = false;

    private Info(){

    }

}
