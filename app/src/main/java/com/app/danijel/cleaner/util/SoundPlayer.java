package com.app.danijel.cleaner.util;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;

import com.app.danijel.cleaner.info.Info;

/**
 * Created by Danijel on 7/14/2017.
 */

public class SoundPlayer {
    private static int currentLength;
    private static MediaPlayer mp;
    private static boolean isMpPaused;

    public static void startBackgroundMusic(Context context, int trackIdentifier) {
        createMusicPlayer(context, trackIdentifier);
        isMpPaused = false;
        if (!mp.isPlaying()) {
            if (!Info.BACKGROUND_MUSIC_ENABLED) {
                return;
            }
            playSound(context, trackIdentifier);
        }
    }

    public static void pauseBackgroundMusic(Context context, int trackIdentifier) {
        try {
            mp.pause();
            currentLength = mp.getCurrentPosition();
            isMpPaused = true;
        } catch (Exception e) {
            createMusicPlayer(context, trackIdentifier);
            if (!Info.BACKGROUND_MUSIC_ENABLED) {
                return;
            }
            mp.start();
            isMpPaused = false;
        }
    }

    public static void continueBackgroundMusic(Context context, int trackIdentifier) {
        if (mp == null) {
            createMusicPlayer(context, trackIdentifier);
        }
        if (!isMpPaused) {
            mp.start();
        } else {
            mp.seekTo(currentLength);
            if (!Info.BACKGROUND_MUSIC_ENABLED) {
                return;
            }
            mp.start();
        }
        isMpPaused = false;
    }

    public static void stopBackgroundMusic() {
        mp.stop();
        mp.reset();
        isMpPaused = false;
    }

    private static void playSound(Context context, int trackIdentifier) {
        try {
            if (mp != null && mp.isPlaying()) {
                mp.stop();
                mp.reset();
            } else {
                createMusicPlayer(context, trackIdentifier);
            }
            if (Info.BACKGROUND_MUSIC_ENABLED) {
                mp.start();
            }
            isMpPaused = false;
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void createMusicPlayer(final Context context, final int trackIdentifier) {
        mp = MediaPlayer.create(context, trackIdentifier);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
            public void onCompletion(MediaPlayer player) {
                mp = null;
                player.release();
                mp = MediaPlayer.create(context, trackIdentifier);
                if (Info.BACKGROUND_MUSIC_ENABLED) {
                    mp.start();
                }
            }
        });
    }

}
