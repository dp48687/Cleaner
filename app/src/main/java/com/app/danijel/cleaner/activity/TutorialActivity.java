package com.app.danijel.cleaner.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.app.danijel.cleaner.R;
import com.app.danijel.cleaner.view.AbstractCustomView;
import com.app.danijel.cleaner.view.TutorialView;

public class TutorialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AbstractCustomView customView = new TutorialView(this, this, R.drawable.cleaner_tutorial, 1550, 2270);
        setContentView(customView);
    }

}
