package com.app.danijel.cleaner.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import com.app.danijel.cleaner.R;
import com.app.danijel.cleaner.util.SoundPlayer;
import com.app.danijel.cleaner.view.PlayView;

public class PlayActivity extends AppCompatActivity {
    private PlayView pv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pv = new PlayView(this, this);
        pv.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        setContentView(pv);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SoundPlayer.continueBackgroundMusic(this, R.raw.track1);
    }

}