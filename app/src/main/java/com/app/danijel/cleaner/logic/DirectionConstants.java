package com.app.danijel.cleaner.logic;

/**
 * Created by Danijel on 7/11/2017.
 */

public enum DirectionConstants {
    UNDEFINED,
    NORTH,
    SOUTH,
    EAST,
    WEST
}
