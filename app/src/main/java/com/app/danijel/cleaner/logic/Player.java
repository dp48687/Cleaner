package com.app.danijel.cleaner.logic;

import com.app.danijel.cleaner.info.Info;

/**
 * Created by Danijel on 7/11/2017.
 */

/**
 * An abstraction of a player.
 * */
public class Player {

    /**
     * The current position of the player.
     * */
    public short x, y;

    /**
     * A previous position is also stored in these variables.
     * */
    public short beforeX, beforeY;

    /**
     * A state in which player is right now.
     * */
    private FieldConstants currentState = FieldConstants.OUTER;

    /**
     * A current direction to which player is headed right now.
     * */
    private DirectionConstants currentDirection = DirectionConstants.UNDEFINED;

    /**
     * Inspects if the stone or cleaned area is up ahead.
     * */
    private boolean isConstraintAhead;

    /**
     * Tries to move the player in direction specified in parameters.
     * @param move a direction in which the player will be moved if the move is legal.
     * @param b current board
     * @return a current state
     * */
    public FieldConstants move(DirectionConstants move, Board b) {
        if (isLegalMove(move, b)) {
            b.setIdentifier(x, y, currentState);
            FieldConstants temp = FieldConstants.CLEANED;
            beforeX = x;
            beforeY = y;
            if (move == DirectionConstants.NORTH) {
                y--;
            } else if (move == DirectionConstants.SOUTH) {
                y++;
            } else if (move == DirectionConstants.EAST) {
                x++;
            } else if (move == DirectionConstants.WEST) {
                x--;
            }
            FieldConstants statePresent = b.getIdentifier(x, y);
            if (currentDirection.equals(DirectionConstants.UNDEFINED)) {
                b.setIdentifier(x, y, FieldConstants.OUTER);
            } else {
                if (currentState.equals(FieldConstants.OUTER)) {
                    b.setIdentifier(beforeX, beforeY, FieldConstants.OUTER);
                } else {
                    b.setIdentifier((short) 0, (short) 0, FieldConstants.OUTER);
                    b.setIdentifier(beforeX, beforeY, temp);
                }
            }
            currentState = statePresent;
            b.setIdentifier(x, y, FieldConstants.PLAYER);
            if (currentState.equals(FieldConstants.REDIRECTOR_EAST)) {
                currentDirection = DirectionConstants.EAST;
            } else if (currentState.equals(FieldConstants.REDIRECTOR_WEST)) {
                currentDirection = DirectionConstants.WEST;
            } else if (currentState.equals(FieldConstants.REDIRECTOR_NORTH)) {
                currentDirection = DirectionConstants.NORTH;
            } else if (currentState.equals(FieldConstants.REDIRECTOR_SOUTH)) {
                currentDirection = DirectionConstants.SOUTH;
            } else {
                currentDirection = move;
            }
            isConstraintAhead = false;
            return statePresent;
        } else {
            isConstraintAhead = true;
            return null;
        }
    }

    /**
     * Tests if the move is legal.
     * @param move a direction in which the player is headed towards
     * @param b a board
     * @return true for a legal move, false otherwise
     */
    private boolean isLegalMove(DirectionConstants move, Board b) {
        short newX = x, newY = y;
        if (move.equals(DirectionConstants.NORTH)) {
            if(currentDirection.equals(DirectionConstants.SOUTH)){
                if ((x == 0 || x == b.getField().length - 1) && y + 1 < b.getField().length) {
                    return true;
                } else {
                    return false;
                }
            }
            newY = (short) (y - 1);
        } else if (move.equals(DirectionConstants.SOUTH)) {
            if(currentDirection.equals(DirectionConstants.NORTH)){
                if ((x == 0 || x == b.getField().length - 1) && (y - 1) > 0) {
                    return true;
                } else {
                    return false;
                }
            }
            newY = (short) (y + 1);
        } else if (move.equals(DirectionConstants.EAST)) {
            if(currentDirection.equals(DirectionConstants.WEST)){
                if ((y == 0 || y == b.getField().length - 1) && x + 1 < b.getField().length) {
                    return true;
                } else {
                    return false;
                }
            }
            newX = (short) (x + 1);
        } else if (move.equals(DirectionConstants.WEST)) {
            if(currentDirection.equals(DirectionConstants.EAST)){
                if ((y == 0 || y == b.getField().length - 1) && x - 1 > 0) {
                    return true;
                } else {
                    return false;
                }
            }
            newX = (short) (x - 1);
        }
        if (newX >= Info.WIDTH || newY >= Info.HEIGHT || newX < 0 || newY < 0) {
            return false;
        }
        if (!b.getIdentifier(newX, newY).equals(FieldConstants.INNER) && !b.getIdentifier(newX, newY).equals(FieldConstants.OUTER)) {
            return false;
        }
        return true;
    }

    /**
     * Tests if the player has legal moves in current board and current state.
     * @param b a board
     * @return true if the legal moves are present, false otherwise
     * */
    public boolean hasLegalMoves(Board b) {
        return isLegalMove(DirectionConstants.NORTH, b) || isLegalMove(DirectionConstants.SOUTH, b) || isLegalMove(DirectionConstants.EAST, b) || isLegalMove(DirectionConstants.WEST, b);
    }

    /**
     * Tests if the constraint is present in front of the player.
     * @return if the constraint is present, false otherwise
     * */
    public boolean isConstraintAhead() {
        return isConstraintAhead;
    }

    /**
     * A simple getter, it retrieves the current state
     * @return current state
     * */
    public FieldConstants getCurrentState() {
        return currentState;
    }

}
